package org.easy.excel.test;

import org.easy.excel.ExcelContext;
import org.easy.excel.result.ExcelImportResult;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
/**
 * Excel导入多行校验测试
 * @author lisuo
 *
 */
public class ImportMultiValidateTest {
	
	// 测试时文件磁盘路径
	private static String path =  "test-excel-error.xlsx";
	// 配置文件路径
	private static ExcelContext context = new ExcelContext("excel-config.xml");
	// Excel配置文件中配置的id
	private static String excelId = "student2";
	
	/**
	 * 导入Excel,使用了org.easy.excel.test.ExportTest.testExportCustomHeader()方法生成的Excel
	 * @throws Exception
	 */
	@Test
	public void testImport()throws Exception{
		Resource resource = new ClassPathResource(path);
		//第二个参数需要注意,它是指标题索引的位置,可能你的前几行并不是标题,而是其他信息,
		//比如数据批次号之类的,关于如何转换成javaBean,具体参考配置信息描述
		ExcelImportResult result = context.readExcel(excelId, 2, resource.getInputStream(),true);
		System.out.println("-------------------header内容-------------------");
		System.out.println(result.getHeader());
		System.out.println("-------------------全量数据-------------------");
		System.out.println(result.getListBean());
		System.out.println("-------------------校验通过的数据-------------------");
		System.out.println(result.getValidListBean());
		System.out.println("-------------------校验为通过的数据-------------------");
		System.out.println(result.getErrorListBean());
		System.out.println("-------------------错误信息描述-------------------");
		System.out.println(result.getErrors());
		resource.getInputStream().close();
		//这种方式和上面的没有任何区别,底层方法默认标题索引为0
		//context.readExcel(excelId, fis);
	}
	
	
		
}
