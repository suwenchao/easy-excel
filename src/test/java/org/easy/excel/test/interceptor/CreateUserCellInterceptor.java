package org.easy.excel.test.interceptor;

import org.easy.excel.aop.cell.CellInterceptorAdapter;
import org.easy.excel.aop.cell.CellJoinPoint;
import org.easy.excel.exception.ExcelException;
import org.easy.excel.test.model.StudentModel;

/**
 * 自定义转换,测试学生创建人
 * @author lisuo
 *
 */
//@Component,与spring集成之后可以使用
public class CreateUserCellInterceptor extends CellInterceptorAdapter{
	
	@Override
	public boolean supports(CellJoinPoint joinPoint) {
		return joinPoint.getValue() != null;
	}
	
	@Override
	public Object executeImport(CellJoinPoint joinPoint) throws ExcelException {
		System.out.println("org.easy.excel.test.converter.CreateUserCellInterceptor输出："+joinPoint.getFieldValue().getOtherConfig());
		if(queryForDb(joinPoint.getValue().toString())){
			//这里可以重新对对象进行设置
			StudentModel stu = (StudentModel) joinPoint.getRefObject();
			stu.setCreateUserId("001");
			//使用accessor同样可以达到上面的效果
//			accessor.setPropertyValue("createUserId", "001");
			//stu.setCreateUserId(xx);
		}else{
			System.out.println("在数据库中没有找到["+joinPoint.getValue()+"]的用户信息");
//			throw new ExcelDataException("在数据库中没有找到["+joinPoint.getValue()+"]的用户信息", joinPoint.getRowNum(), joinPoint.getFieldValue().getAlias(),joinPoint.getOriginalValue(),joinPoint.getRefObject());
		}
		return super.executeImport(joinPoint);
	}
	
	@Override
	public Object executeExport(CellJoinPoint joinPoint) throws ExcelException {
		System.out.println("第【"+joinPoint.getRowNum()+"】行Cell数据被创建");
		return super.executeExport(joinPoint);
	}

	
	//模拟查询数据库
	private boolean queryForDb(String createUser){
		if(createUser.startsWith("王五")){
			System.out.println("数据库有查询到王五");
			return true;
		}
		return false;
	}
	
}
