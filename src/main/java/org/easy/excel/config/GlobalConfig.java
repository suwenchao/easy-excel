package org.easy.excel.config;

import java.util.ArrayList;
import java.util.List;

import org.easy.excel.aop.cell.CellInterceptor;
import org.easy.excel.aop.cell.inner.AfterCellInterceptor;
import org.easy.excel.aop.cell.inner.CustomCellInterceptor;
import org.easy.excel.aop.cell.inner.DecimalFormatCellInterceptor;
import org.easy.excel.aop.cell.inner.DefaultValueCellInterceptor;
import org.easy.excel.aop.cell.inner.FormatCellInterceptor;
import org.easy.excel.aop.cell.inner.IsNullCellInterceptor;
import org.easy.excel.aop.cell.inner.PatternCellInterceptor;
import org.easy.excel.aop.cell.inner.RegexCellInterceptor;
import org.easy.excel.aop.cell.inner.TrimStringCellInterceptor;

/**
 * Excel 全局配置
 * @author lisuo
 *
 */
public class GlobalConfig {
	
	private List<CellInterceptor> cellInterceptors = new ArrayList<>();
	
	private static GlobalConfig instance = new GlobalConfig();
	
	private GlobalConfig() {
		cellInterceptors.add(new DefaultValueCellInterceptor());
		cellInterceptors.add(new TrimStringCellInterceptor());
		cellInterceptors.add(new IsNullCellInterceptor());
		cellInterceptors.add(new RegexCellInterceptor());
		cellInterceptors.add(new PatternCellInterceptor());
		cellInterceptors.add(new FormatCellInterceptor());
		cellInterceptors.add(new DecimalFormatCellInterceptor());
		cellInterceptors.add(new AfterCellInterceptor());
		cellInterceptors.add(new CustomCellInterceptor());
	}
	
	public static GlobalConfig getInstance() {
		return instance;
	}
	
	public List<CellInterceptor> getCellInterceptors() {
		return cellInterceptors;
	}
	
}
